import React, { Component } from 'react';
import './App.css';

const AuthContext = React.createContext()
const Provider = AuthContext.Provider
const Consumer = AuthContext.Consumer

const Title = props => {

  return <Consumer>{auth => {
    if (!auth.isAuth) {
      return <h1>Deslogado!</h1>
    }

    return <div>{auth.user}</div>
  }}</Consumer>

  // if (!props.auth.isAuth) {
  //   return <h1>Deslogado!</h1>
  // }

  // return <div>{props.auth.user}</div>
}

const Header = props => {
  return <Title />
  // return <Consumer>{auth => auth.user}</Consumer>
  // return <Title auth={props.auth} />
}

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      auth: {
        user: null,
        isAuth: false
      }
    }
  }

  render() {
    return (
      <Provider value={this.state.auth}>
        <div className="App">
          <div className="App-intro">
            Welcome

            <Header />
            {/*<Header auth={this.state.auth} />*/}

            <button onClick={() => {
              if (this.state.auth.isAuth) {
                this.setState({ auth: { user: null, isAuth: false } })
              } else {
                this.setState({ auth: { user: 'anderson', isAuth: true } })
              }
            }}>Auth</button>
          </div>
        </div>
      </Provider>
    );
  }
}

export default App;
